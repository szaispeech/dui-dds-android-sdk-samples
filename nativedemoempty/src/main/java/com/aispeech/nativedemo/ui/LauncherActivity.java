package com.aispeech.nativedemo.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.aispeech.nativedemo.R;

import java.util.ArrayList;
import java.util.List;

public class LauncherActivity extends Activity {

    private static final String TAG = "LauncherActivity";

    private String[] mPermissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CONTACTS,
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.ACCESS_COARSE_LOCATION,
//            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        checkAndRequestPermission();
    }

    private void checkAndRequestPermission() {
        Log.i(TAG, "checkAndRequestPermission");
        //实例化动态权限申请类
        boolean granted = true;
        for (String permission : mPermissions) {
            int g = ActivityCompat.checkSelfPermission(this, permission);
            Log.i(TAG, "permission: " + permission + "  " + g);
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                granted = false;
            }
        }
        if (!granted) {
            requestPermission(); // 所有权限一同申请
        } else {
            gotoMainActivity();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                mPermissions, 1);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1) {
            List<String> notGranted = new ArrayList<>();
            for (int i = 0; i < permissions.length; i++) {
                Log.i(TAG, "onRequestPermissionsResult: " + permissions[i] + " " + grantResults[i]);
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    notGranted.add(permissions[i]);
                }
            }

            if (notGranted.contains("android.permission.RECORD_AUDIO") || notGranted.contains("android.permission.READ_CONTACTS")) {
                Toast.makeText(this, "至少需要 录音、读取联系人 权限！！", Toast.LENGTH_SHORT).show();
                LauncherActivity.this.finish();
            }
        }
    }

    // 跳转到主页面
    private void gotoMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

}
