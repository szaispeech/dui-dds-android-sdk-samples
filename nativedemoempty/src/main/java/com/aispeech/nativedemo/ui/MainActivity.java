package com.aispeech.nativedemo.ui;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.DDSAuthListener;
import com.aispeech.dui.dds.DDSConfig;
import com.aispeech.dui.dds.DDSInitListener;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.nativedemo.R;
import com.aispeech.nativedemo.observer.DuiUpdateObserver;

public class MainActivity extends AppCompatActivity  implements DuiUpdateObserver.UpdateCallback{
    public static final String TAG = "MainActivity";
    Handler mHandler = new Handler();
    private DuiUpdateObserver mUpdateObserver = new DuiUpdateObserver();// dds更新监听器
    // dds初始状态监听器,监听init是否成功
    private DDSInitListener mInitListener = new DDSInitListener() {
        @Override
        public void onInitComplete(boolean isFull) {
            Log.d(TAG, "onInitComplete " + isFull);
            if (isFull) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            DDS.getInstance().getAgent().getWakeupEngine().enableWakeup();
                        } catch (DDSNotInitCompleteException e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);
            }
        }

        @Override
        public void onError(int what, final String msg) {
            Log.e(TAG, "Init onError: " + what + ", error: " + msg);
        }
    };
    // dds认证状态监听器,监听auth是否成功
    private DDSAuthListener mAuthListener = new DDSAuthListener() {
        @Override
        public void onAuthSuccess() {
            Log.d(TAG, "onAuthSuccess");
        }

        @Override
        public void onAuthFailed(final String errId, final String error) {
            Log.e(TAG, "onAuthFailed: " + errId + ", error:" + error);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    // 初始化dds组件
    private void init() {
        //DDS.getInstance().setDebugMode(2); //在调试时可以打开sdk调试日志，在发布版本时，请关闭
        DDS.getInstance().init(getApplicationContext(), createConfig(), mInitListener, mAuthListener);
    }

    // 创建dds配置信息
    private DDSConfig createConfig() {
        // 用户设置自己实现的单个功能，目前支持 wakeup 和 vad。WakeupII.class 是一个使用示例
        // DDS.getInstance().setOutsideEngine(IEngine.Name.WAKEUP_SINGLE_MIC, WakeupII.class);
        DDSConfig config = new DDSConfig();
        // 基础配置项
        config.addConfig(DDSConfig.K_PRODUCT_ID, "278578029"); // 产品ID -- 必填
        config.addConfig(DDSConfig.K_USER_ID, "aispeech");  // 用户ID -- 必填
        config.addConfig(DDSConfig.K_ALIAS_KEY, "pro");   // 产品的发布分支 -- 必填
        config.addConfig(DDSConfig.K_PRODUCT_KEY, "f51edbf74a8a3bf64083ca9717c3bc76");// Product Key -- 必填
        config.addConfig(DDSConfig.K_PRODUCT_SECRET, "987181c998312a3ceb46d725aa0e47cb");// Product Secre -- 必填
        config.addConfig(DDSConfig.K_API_KEY, "0f1093799bbf9a32203966c2612467dc");  // 产品授权秘钥，服务端生成，用于产品授权 -- 必填
        config.addConfig(DDSConfig.K_DEVICE_ID, "deveceid");//填入唯一的deviceId -- 选填

        // 更多高级配置项,请参考文档: https://www.dui.ai/docs/ct_common_Andriod_SDK 中的 --> 四.高级配置项

        config.addConfig(DDSConfig.K_DUICORE_ZIP, "duicore.zip"); // 预置在指定目录下的DUI内核资源包名, 避免在线下载内核消耗流量, 推荐使用
        config.addConfig(DDSConfig.K_USE_UPDATE_DUICORE, "true"); //设置为false可以关闭dui内核的热更新功能，可以配合内置dui内核资源使用
        config.addConfig(DDSConfig.K_CUSTOM_ZIP, "product.zip");
        // 麦克风阵列配置项
        config.addConfig(DDSConfig.K_MIC_TYPE, "0"); // 设置硬件采集模组的类型 0：无。默认值。 1：单麦回消 2：线性四麦 3：环形六麦 4：车载双麦 5：家具双麦 6: 环形四麦  7: 新车载双麦 8: 线性6麦
        config.addConfig(DDSConfig.K_USE_NEAR_WAKEUP, "false");


        Log.i(TAG, "config->" + config.toString());
        return config;
    }

    public void click1(View view) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    DDS.getInstance().getAgent().getWakeupEngine().enableWakeup();
                } catch (DDSNotInitCompleteException e) {
                    e.printStackTrace();
                }
            }
        }, 500);
    }

    public void click2(View view) {
        DDS.getInstance().releaseSync();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                init();
            }
        },200);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUpdateObserver.regist(this);
    }

    @Override
    public void onUpdate(int type, String result) {
        Log.d(TAG, "onUpdate: type " + type + " result " + result);
    }

    @Override
    protected void onDestroy() {
        mUpdateObserver.unregist();
        super.onDestroy();
    }
}
