package com.aispeech.nativedemo.music;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aispeech.nativedemo.R;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PlayerActivity extends AppCompatActivity {

    private static final String TAG = "PlayerActivity";


    protected AppCompatImageView playerType;
    protected AppCompatImageView playerPause;
    protected AppCompatImageView playerPre;
    protected AppCompatImageView playerNext;
    protected AppCompatImageView playerCollect;
    protected AppCompatImageView playerList;
    protected NavigationView navigationView;
    protected DrawerLayout drawerLayout;
    protected TextView playerName;
    protected TextView playerArtist;
    protected ImageView playerAlbum;
    protected RecyclerView recyclerView;
    TextView playerProgress;
    TextView playerDuration;
    AppCompatSeekBar seekBar;

    private List<Music> musicList;
    private Gson mGson;
    private Player player;
    private DetailAdapter adapter;
    private Disposable timer;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String data = intent.getStringExtra("data");
        musicList = loadMusic(data);
        if (player != null) {
            player.stop();
            player = null;
        }
        init();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.player_main);
        playerType = findViewById(R.id.playerType);
        playerPause = findViewById(R.id.playerPause);
        playerPre = findViewById(R.id.playerPre);
        playerNext = findViewById(R.id.playerNext);
        playerCollect = findViewById(R.id.playerFavorite);
        playerList = findViewById(R.id.playerList);
        navigationView = findViewById(R.id.nav_view);
        drawerLayout = findViewById(R.id.drawerLayout);
        playerName = findViewById(R.id.playerName);
        playerArtist = findViewById(R.id.playerArtist);
        playerAlbum = findViewById(R.id.playerAlbum);
        recyclerView = findViewById(R.id.recyclerView);
        playerProgress = findViewById(R.id.playerProgress);
        playerDuration = findViewById(R.id.playerDuration);
        seekBar = findViewById(R.id.playerSeekBar);

        playerType.setOnClickListener(clickListener);
        playerPause.setOnClickListener(clickListener);
        playerPre.setOnClickListener(clickListener);
        playerNext.setOnClickListener(clickListener);
        playerCollect.setOnClickListener(clickListener);
        playerList.setOnClickListener(clickListener);

        mGson = new Gson();
        Intent intent = getIntent();
        String data = intent.getStringExtra("data");
        musicList = loadMusic(data);
        init();
    }

    private void init() {
        if (musicList != null && musicList.size() > 0) {
            player = Player.getInstance();
            player.init(musicList);
            player.setOnPreparedListener(mp -> {
                if (mp != null) {
                    mp.start();
                }
                if (!TextUtils.isEmpty(player.getCurrentMusic().getImageUrl())) {
                    Observable.just("")
                            .observeOn(Schedulers.io())
                            .subscribe(o -> {
                                Bitmap bitmap = Picasso.get().load(player.getCurrentMusic().getImageUrl()).get();
                                runOnUiThread(() -> playerAlbum.setImageBitmap(bitmap));
                            }, throwable -> Log.e(TAG, throwable.getLocalizedMessage(), throwable));
                } else {
                    playerAlbum.setImageResource(R.drawable.default_record_album);
                }
                playerName.setText(player.getCurrentMusic().getTitle());
                if (!TextUtils.isEmpty(player.getCurrentMusic().getSubTitle())) {
                    playerArtist.setText(player.getCurrentMusic().getSubTitle());
                } else {
                    playerArtist.setText("");
                }
                playerPause.setImageResource(R.drawable.ic_pause);
                timer = Observable.interval(1, TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.computation())
                        .subscribe(__ -> {
                            if (player != null) {
                                int d = player.getProgress();
                                String duration = Player.formatDuration(player.getCurrentDuration());
                                playerProgress.setText(duration);
                                seekBar.setProgress(d);
                            }
                        });
                playerDuration.setText(Player.formatDuration(player.getDuration()));
                seekBar.setMax(player.getDuration() / 1000);
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            player.seekTo(progress * 1000);
                            playerProgress.setText(Player.formatDuration(player.getCurrentDuration()));
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                if (LocalMusicSp.get().isFavorite(player.getCurrentMusic().getTitle())) {
                    playerCollect.setImageResource(R.drawable.ic_favorite_yes);
                } else {
                    playerCollect.setImageResource(R.drawable.ic_favorite_no);
                }
                switch (LocalMusicSp.get().getInt("playMode")) {
                    case Player.PlayMode.PLAY_MODE_LIST:
                        playerType.setImageResource(R.drawable.ic_play_mode_list);
                        break;
                    case Player.PlayMode.PLAY_MODE_REPEAT_LIST:
                        playerType.setImageResource(R.drawable.ic_play_mode_loop);
                        break;
                    case Player.PlayMode.PLAY_MODE_REPEAT_ONE:
                        playerType.setImageResource(R.drawable.ic_play_mode_single);
                        break;
                    case Player.PlayMode.PLAY_MODE_SHUFFLE:
                        playerType.setImageResource(R.drawable.ic_play_mode_shuffle);
                        break;
                    default:
                }
            });
            adapter = new DetailAdapter(musicList);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            recyclerView.setAdapter(adapter);
            Log.d(TAG, "initComplete");
        }
    }

    private List<Music> loadMusic(String data) {
        try {
            Log.d(TAG, "loadMusic: " + data);
            JSONObject object = new JSONObject(data);
            Music[] musics = mGson.fromJson(object.optJSONArray("content").toString(), Music[].class);
            return Arrays.asList(musics);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public void playerTypeClick() {
        int mode = player.getPlayMode();
        switch (mode) {
            case Player.PlayMode.PLAY_MODE_LIST:
                playerType.setImageResource(R.drawable.ic_play_mode_loop);
                player.setPlayMode(Player.PlayMode.PLAY_MODE_REPEAT_LIST);
                LocalMusicSp.get().put("playMode", 1);
                break;
            case Player.PlayMode.PLAY_MODE_REPEAT_LIST:
                playerType.setImageResource(R.drawable.ic_play_mode_single);
                player.setPlayMode(Player.PlayMode.PLAY_MODE_REPEAT_ONE);
                LocalMusicSp.get().put("playMode", 2);
                break;
            case Player.PlayMode.PLAY_MODE_REPEAT_ONE:
                playerType.setImageResource(R.drawable.ic_play_mode_shuffle);
                player.setPlayMode(Player.PlayMode.PLAY_MODE_SHUFFLE);
                LocalMusicSp.get().put("playMode", 3);
                break;
            case Player.PlayMode.PLAY_MODE_SHUFFLE:
                playerType.setImageResource(R.drawable.ic_play_mode_list);
                player.setPlayMode(Player.PlayMode.PLAY_MODE_LIST);
                LocalMusicSp.get().put("playMode", 0);
                break;
            default:
        }
    }

    public void playerPauseClick() {
        if (player.isPlaying()) {
            player.pause();
            playerPause.setImageResource(R.drawable.ic_play);
        } else {
            player.resume();
            playerPause.setImageResource(R.drawable.ic_pause);
        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.playerType:
                    playerTypeClick();
                    break;
                case R.id.playerPause:
                    playerPauseClick();
                    break;
                case R.id.playerPre:
                    playerPreClick();
                    break;
                case R.id.playerNext:
                    playerNextClick();
                    break;
                case R.id.playerFavorite:
                    playerCollectClick();
                    break;
                case R.id.playerList:
                    playerListClick();
                    break;
            }
        }
    };

    public void playerPreClick() {
        player.prev();
    }

    public void playerNextClick() {
        player.next();
    }

    public void playerCollectClick() {
        if (player.isFavorite()) {
            playerCollect.setImageResource(R.drawable.ic_favorite_no);
            player.unFavorite();
        } else {
            playerCollect.setImageResource(R.drawable.ic_favorite_yes);
            player.favorite();
        }
    }

    public void playerListClick() {
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.stop();
        player = null;
        mGson = null;
        musicList = null;
        if (timer != null) {
            timer.dispose();
        }
    }

    class DetailAdapter extends RecyclerView.Adapter {

        List<Music> list;

        public DetailAdapter(List<Music> list) {
            this.list = list;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(PlayerActivity.this)
                    .inflate(R.layout.detail_item, viewGroup, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
            final VH vh = (VH) viewHolder;
            vh.textView.setText(list.get(i).getTitle());
            if (!TextUtils.isEmpty(list.get(i).getSubTitle())) {
                vh.textSinger.setText(list.get(i).getSubTitle());
            } else {
                vh.textSinger.setText("");
            }
            vh.layout.setOnClickListener(v -> player.playByIndex(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class VH extends RecyclerView.ViewHolder {

            public TextView textView;
            public TextView textSinger;
            public LinearLayout layout;

            public VH(View view) {
                super(view);
                textView = view.findViewById(R.id.songName);
                textSinger = view.findViewById(R.id.singer);
                layout = view.findViewById(R.id.layout);
            }
        }
    }

}
