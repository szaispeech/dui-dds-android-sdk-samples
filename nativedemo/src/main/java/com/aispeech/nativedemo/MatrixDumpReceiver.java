package com.aispeech.nativedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tencent.matrix.hook.memory.MemoryHook;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MatrixDumpReceiver extends BroadcastReceiver {
    private SimpleDateFormat timesdf = new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss");

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == null)
            return;

        if ("com.aispeech.matrix_dump".equals(intent.getAction())) {
            File externalDir = context.getExternalCacheDir();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    File folder = new File(externalDir, "/memory");
                    if (!folder.exists()) {
                        folder.mkdir();
                    }
                    String fileTime = timesdf.format(new Date());
                    String logPath = new File(folder, "DumpLog_" + fileTime + ".log").getAbsolutePath();
                    String jsonPath = new File(folder, "DumpJson_" + fileTime + ".log").getAbsolutePath();
                    Log.d("MatrixDumpReceiver", "logPath: " + logPath);
                    Log.d("MatrixDumpReceiver", "jsonPath: " + jsonPath);
                    MemoryHook.INSTANCE.dump(logPath, jsonPath);
                }
            }).start();
        }
    }
}