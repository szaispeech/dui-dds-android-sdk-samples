package com.aispeech.nativedemo;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.DDSErrorListener;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.tencent.matrix.Matrix;
import com.tencent.matrix.hook.HookManager;
import com.tencent.matrix.hook.memory.MemoryHook;
import com.tencent.matrix.hook.pthread.PthreadHook;
import com.tencent.matrix.iocanary.IOCanaryPlugin;
import com.tencent.matrix.iocanary.config.IOConfig;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

public class DuiApplication extends Application {

    private static final String TAG = "application";
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        Fresco.initialize(this);
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);

        /*Matrix.Builder builder = new Matrix.Builder(this); // build matrix
        builder.pluginListener(new TestPluginListener(this)); // add general pluginListener
        DynamicConfigImplDemo dynamicConfig = new DynamicConfigImplDemo(); // dynamic config

        // init plugin
        IOCanaryPlugin ioCanaryPlugin = new IOCanaryPlugin(new IOConfig.Builder()
                .dynamicConfig(dynamicConfig)
                .build());
        //add to matrix
        builder.plugin(ioCanaryPlugin);

        //init matrix
        Matrix.init(builder.build());

        // start plugin
        ioCanaryPlugin.start();*/

        Matrix.Builder builder = new Matrix.Builder(this);
        Matrix.init(builder.build());
        initMatrix();
       // MemoryHook.INSTANCE.addHookSo(".*libvad\\.so$" );
        // MemoryHook.INSTANCE.enableStacktrace(false);
        // 手动dump，导出so的内存分配信息
        // MemoryHook.INSTANCE.dump(output, outputJson)
        // adb shell am broadcast -a "com.aispeech.matrix_dump"
    }

    private void initMatrix() {
        // Init Hooks.
        try {
            //配置检测 Android Java 和 native 线程泄漏及缩减 native 线程栈空间的工具
            PthreadHook.INSTANCE
                    .addHookThread(".*")
                    .setThreadTraceEnabled(true)
                    .enableTracePthreadRelease(true)
                    .enableQuicken(false);

            String threadOutput = getExternalCacheDir() + "/pthread_hook.log";
            PthreadHook.INSTANCE.dump(threadOutput);
            PthreadHook.INSTANCE.enableLogger(false);

            // 配置检测 Android native 内存泄漏的工具
            MemoryHook.INSTANCE
                    //单独配置so的方式 ".*libasr\\.so$",".*libgram\\.so$",".*libvad\\.so$"
                    .addHookSo("lib.*\\.so$") //全匹配的方式 ".*com\\.byd\\.dm.*\\.so$"
                    .enableStacktrace(true)
                    .stacktraceLogThreshold(0)
                    .enableMmapHook(true);
            HookManager.INSTANCE
                    // Memory hook
                    .addHook(MemoryHook.INSTANCE)
                    // Thread hook
                    .addHook(PthreadHook.INSTANCE)
                    .commitHooks();

        } catch (HookManager.HookFailedException e) {
            e.printStackTrace();
        }
        Log.d("MatrixDumpReceiver", "MemoryHook.INSTANCE.getStatus(): " + MemoryHook.INSTANCE.getStatus());
    }

    public static Context getContext() {
        if (mContext == null) {
            throw new RuntimeException("Unknown Error");
        }
        return mContext;
    }

    public static void setErrorListener() {
        DDS.getInstance().setDDSErrorListener(new DDSErrorListener() {
            @Override
            public void onError(int code, JSONObject jsonObject) {
                Log.e(TAG, "这个接口暂时不会有回调出来");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mContext.startService(DDSService.newDDSServiceIntent(mContext, "stop"));
                        mContext.startService(DDSService.newDDSServiceIntent(mContext, "start"));
                    }
                }).start();
            }
        });
    }

    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            if (e instanceof OutOfMemoryError) {
                Log.d(TAG, "uncaughtException(): " + e.toString());
                long freeMemory = Runtime.getRuntime().freeMemory();
                long totalMemory = Runtime.getRuntime().totalMemory();
                long maxMemory = Runtime.getRuntime().maxMemory();
                int availableProcessors = Runtime.getRuntime().availableProcessors();
                Log.d(TAG, "freeMemory " + freeMemory);
                Log.d(TAG, "totalMemory " + totalMemory);
                Log.d(TAG, "maxMemory " + maxMemory);
                Log.d(TAG, "availableProcessors " + availableProcessors);

                Log.d(TAG, "status file:\n" + readSystemFile("/proc/" + android.os.Process.myPid() + "/status"));
                Log.d(TAG, "limits file:\n" + readSystemFile("/proc/" + android.os.Process.myPid() + "/limits"));
                Log.d(TAG, "threads-max file:\n" + readSystemFile("/proc/sys/kernel/threads-max"));

                int pid = android.os.Process.myPid();
                Log.d(TAG, "pid: " + pid);
                File fd = new File("/proc/" + pid + "/fd");
                if (fd.exists() && fd.isDirectory()) {
                    File[] files = fd.listFiles();
                    if (files != null && files.length > 0) {
                        Log.d(TAG, "fd 目录下有 " + files.length + " 个文件");
                        Log.d(TAG, String.format("%10S  %10S  %10S  %10S", "name", "isFile", "isDirectory", "length"));
                        for (File f : files) {
                            Log.d(TAG, String.format("%10S  %10S  %10S  %10S",
                                    f.getName(), f.isFile() ? f.isFile() : "", f.isDirectory() ? f.isDirectory() : "", f.isFile() ? f.length() : ""));
                        }
                    }
                }
            }
        }
    };

    private static String readSystemFile(String filePath) {
        StringBuffer sb = new StringBuffer();
        File threadsMaxFile = new File(filePath);
        try {
            FileInputStream fileInputStream = new FileInputStream(threadsMaxFile);

            byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1) {
                sb.append(new String(bytes, 0, len));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
