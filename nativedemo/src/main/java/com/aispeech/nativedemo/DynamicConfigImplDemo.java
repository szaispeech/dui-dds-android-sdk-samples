package com.aispeech.nativedemo;

import android.util.Log;

import com.tencent.mrs.plugin.IDynamicConfig;

public class DynamicConfigImplDemo implements IDynamicConfig {
    private static final String TAG = "DynamicConfigImplDemo";

    public DynamicConfigImplDemo() {
    }

    public boolean isFPSEnable() {
        Log.i(TAG, "isFPSEnable: " + true);
        return true;
    }

    public boolean isTraceEnable() {
        Log.i(TAG, "isTraceEnable: " + true);
        return true;
    }

    public boolean isMatrixEnable() {
        Log.i(TAG, "isMatrixEnable: " + true);
        return true;
    }

    public boolean isDumpHprof() {
        Log.i(TAG, "isDumpHprof: " + false);
        return false;
    }

    @Override
    public String get(String key, String defStr) {
        //hook to change default values
        Log.i(TAG, "key: " + key + " defStr: " + defStr);
        return defStr;
    }

    @Override
    public int get(String key, int defInt) {
        //hook to change default values
        Log.i(TAG, "key: " + key + " defInt: " + defInt);
        return defInt;
    }

    @Override
    public long get(String key, long defLong) {
        //hook to change default values
        Log.i(TAG, "key: " + key + " defLong: " + defLong);
        return defLong;
    }

    @Override
    public boolean get(String key, boolean defBool) {
        //hook to change default values
        Log.i(TAG, "key: " + key + " defBool: " + defBool);
        return defBool;
    }

    @Override
    public float get(String key, float defFloat) {
        //hook to change default values
        Log.i(TAG, "key: " + key + " defFloat: " + defFloat);
        return defFloat;
    }
}
