package com.aispeech.nativedemo.ui;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.nativedemo.DDSService;
import com.aispeech.nativedemo.R;

import java.util.ArrayList;
import java.util.List;

public class LauncherActivity extends Activity {

    private static final String TAG = "LauncherActivity";
    private AlertDialog mDialog;
    private int mAuthCount = 0;// 授权次数,用来记录自动授权

    private String[] mPermissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CONTACTS,
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.ACCESS_COARSE_LOCATION,
//            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        checkAndRequestPermission();
    }

    private void checkAndRequestPermission() {
        Log.i(TAG, "checkAndRequestPermission");
        //实例化动态权限申请类
        boolean granted = true;
        for (String permission : mPermissions) {
            int g = ActivityCompat.checkSelfPermission(this, permission);
            Log.i(TAG, "permission: " + permission + "  " + g);
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                granted = false;
            }
        }
        if (!granted) {
            requestPermission(); // 所有权限一同申请
        } else {
            startDDSService();
            new Thread() {
                public void run() {
                    checkDDSReady();
                }
            }.start();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                mPermissions, 1);
    }

    private void startDDSService() {
        Log.i(TAG, "startDDSService");
        startService(DDSService.newDDSServiceIntent(this, "start"));
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1) {
            List<String> notGranted = new ArrayList<>();
            for (int i = 0; i < permissions.length; i++) {
                Log.i(TAG, "onRequestPermissionsResult: " + permissions[i] + " " + grantResults[i]);
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    notGranted.add(permissions[i]);
                }
            }

            if (notGranted.contains("android.permission.RECORD_AUDIO") || notGranted.contains("android.permission.READ_CONTACTS")) {
                Toast.makeText(this, "至少需要 录音、读取联系人 权限！！", Toast.LENGTH_SHORT).show();
                LauncherActivity.this.finish();
            } else {
                startDDSService();
                new Thread() {
                    public void run() {
                        checkDDSReady();
                    }
                }.start();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 注册一个广播,接收service中发送的dds初始状态广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ddsdemo.intent.action.auth_failed");// 认证失败的广播
        registerReceiver(authReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 释放注册的广播
        unregisterReceiver(authReceiver);
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        finish();
    }

    // 检查dds是否初始成功
    public void checkDDSReady() {
        while (true) {
            if (DDS.getInstance().getInitStatus() == DDS.INIT_COMPLETE_FULL ||
                    DDS.getInstance().getInitStatus() == DDS.INIT_COMPLETE_NOT_FULL) {
                if (DDS.getInstance().isAuthSuccess()) {
                    gotoMainActivity();
                    break;
                } else {
                    // 自动授权
                    doAutoAuth();
                }
                break;
            } else {
                Log.w(TAG, "waiting  init complete finish...");
            }
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // 跳转到主页面
    private void gotoMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    // 显示授权弹框给用户
    private void showDoAuthDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(LauncherActivity.this);
                builder.setMessage("未授权");
                builder.setPositiveButton("做一次授权", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            DDS.getInstance().doAuth();
                        } catch (DDSNotInitCompleteException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                });

                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                mDialog = builder.create();
                mDialog.show();
            }
        });
    }

    // 认证广播
    private BroadcastReceiver authReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), "ddsdemo.intent.action.auth_failed")) {
                doAutoAuth();
            }
        }
    };

    // 执行自动授权
    private void doAutoAuth() {
        // 自动执行授权5次,如果5次授权失败之后,给用户弹提示框
        if (mAuthCount < 5) {
            try {
                DDS.getInstance().doAuth();
                mAuthCount++;
            } catch (DDSNotInitCompleteException e) {
                e.printStackTrace();
            }
        } else {
            showDoAuthDialog();
        }
    }
}
