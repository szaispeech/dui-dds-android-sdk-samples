package com.aispeech.test.increment_wakeup;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.Agent;
import com.aispeech.dui.dds.agent.wakeup.word.WakeupWord;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

import java.util.ArrayList;
import java.util.List;

public class incrementWakeupActivity extends BaseTestActivity {

    @Override
    protected void initData() {
        mData.add(new ListItem<>("添加命令唤醒词", "addCommandWord"));
        mData.add(new ListItem<>("更新命令唤醒词", "updateCommandWord"));
        mData.add(new ListItem<>("删除命令唤醒词", "removeCommand"));
        mData.add(new ListItem<>("清空命令唤醒词", "clearCommandWord"));

        mData.add(new ListItem<>("添加打断唤醒词", "addShortCutWord"));
        mData.add(new ListItem<>("更新打断唤醒词", "updateShortCutWord"));
        mData.add(new ListItem<>("删除打断唤醒词", "removeShortCutWord"));
        mData.add(new ListItem<>("清空打断唤醒词", "clearShortCutWord"));

        mData.add(new ListItem<>("清空所有增量唤醒词", "clearAllWakeupWord"));
    }

    public void addCommandWord() throws DDSNotInitCompleteException {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
                .addGreeting("我在")
                .setAction("sys.next")
                .setIncrementWord("true");
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
                .addGreeting("我在")
                .setAction("sys.play")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        DDS.getInstance().getAgent().getWakeupEngine().addCommandWakeupWords(commandList);
        toMain();
    }

    public void updateCommandWord() throws DDSNotInitCompleteException {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
                .addGreeting("我在呢")
                .setAction("sys.next")
                .setIncrementWord("true");;
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
                .addGreeting("我在呢")
                .setAction("sys.play")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        DDS.getInstance().getAgent().getWakeupEngine().updateCommandWakeupWords(commandList);
        toMain();
    }

    public void removeCommand() {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
//                .setPinyin("xia yi shou")
                .setIncrementWord("true");;
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
//                .setPinyin("shang yi shou")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeCommandWakeupWords(commandList);
        } catch (DDSNotInitCompleteException e) {
//            Log.d("xlg","--------------error---------");
            e.printStackTrace();
        }

        toMain();
    }

    public void clearCommandWord() throws DDSNotInitCompleteException {
        //清空当前设置的增量命令唤醒词
        DDS.getInstance().getAgent().getWakeupEngine().clearIncrementCommandWakeupWord();
        toMain();
    }

    //打断唤醒词

    public void addShortCutWord() throws DDSNotInitCompleteException {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
                .setIncrementWord("true");;
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        DDS.getInstance().getAgent().getWakeupEngine().addShortcutWakeupWords(commandList);
        toMain();
    }

    public void updateShortCutWord() throws DDSNotInitCompleteException {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
                .setIncrementWord("true");;
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        DDS.getInstance().getAgent().getWakeupEngine().updateShortcutWakeupWords(commandList);
        toMain();
    }

    public void removeShortCutWord() throws DDSNotInitCompleteException {
        WakeupWord commandWord = new WakeupWord()
                .setWord("下一首")
                .setIncrementWord("true");
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("上一首")
                .setIncrementWord("true");;
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        DDS.getInstance().getAgent().getWakeupEngine().removeShortcutWakeupWords(commandList);
        toMain();

    }

    public void clearShortCutWord() throws DDSNotInitCompleteException {
        //清空当前设置的增量打断唤醒词
        DDS.getInstance().getAgent().getWakeupEngine().clearIncrementShortCutWakeupWord();
        toMain();
    }


    public void clearAllWakeupWord() throws DDSNotInitCompleteException {
        // 清空当前设置的所有增量唤醒词
        DDS.getInstance().getAgent().getWakeupEngine().clearIncrementWakeupWord();
        toMain();
    }



}

