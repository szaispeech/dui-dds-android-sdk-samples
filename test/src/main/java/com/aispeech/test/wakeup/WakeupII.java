package com.aispeech.test.wakeup;

import com.aispeech.libcomm.abslite.IWakeupEngine;
import com.aispeech.libcomm.bean.wakeup.WakeupCfg;
import com.aispeech.libcomm.bean.wakeup.WordBean;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

public class WakeupII implements IWakeupEngine {

    private OnWakeupListener onWakeupListener;
    private int count;

    @Override
    public void init(WakeupCfg cfg, OnWakeupListener onWakeupListener) {
        this.onWakeupListener = onWakeupListener;
        onWakeupListener.onInit(0);
    }

    @Override
    public void start(WakeupCfg.WakeupParam param) {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void feed(byte[] pcm) {
        count++;
        // feed 100 次有一次唤醒
        if (count % 100 == 0) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("wakeupWord", "ni hao xiao chi");
                jsonObject.put("confidence", 0.1698);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            onWakeupListener.onResult(jsonObject.toString());
            count = 0;
            onWakeupListener.onResultDataReceived(pcm, pcm.length, 1);
        } else
            onWakeupListener.onResultDataReceived(pcm, pcm.length, 0);
    }

    @Override
    public int getValueOf(String param) {
        return 0;
    }

    @Override
    public void set(String jsonStr) {

    }

    @Override
    public void setWakeupWords(Collection<WordBean> words) {

    }

    @Override
    public void setBlackWords(String[] blackWords) {

    }

    @Override
    public String getNewConf() {
        return "";
    }

    @Override
    public String getStartConf() {
        return "";
    }

    @Override
    public void resetDriveMode() {

    }
}
