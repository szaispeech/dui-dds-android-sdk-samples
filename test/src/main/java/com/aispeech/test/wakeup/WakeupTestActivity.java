package com.aispeech.test.wakeup;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.wakeup.WakeupCallback;
import com.aispeech.dui.dds.agent.wakeup.cb.BfListener;
import com.aispeech.dui.dds.agent.wakeup.cb.VoipListener;
import com.aispeech.dui.dds.agent.wakeup.word.WakeupType;
import com.aispeech.dui.dds.agent.wakeup.word.WakeupWord;
import com.aispeech.dui.dds.agent.wakeup.word.WakeupWordIntent;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WakeupTestActivity extends BaseTestActivity {
    @Override
    protected void initData() {
        mData.add(new ListItem("开启语音唤醒", "enableWakeup"));
        mData.add(new ListItem("关闭语音唤醒", "disableWakeup"));
        mData.add(new ListItem("添加一条主唤醒词-小明", "addMain"));
        mData.add(new ListItem("添加一条主唤醒词-小美", "addMain1"));
        mData.add(new ListItem("删除一条主唤醒词-小明", "removeMainWakeupWord"));
        mData.add(new ListItem("添加多条主唤醒词-小红/天天", "addMainWakeupWords"));
        mData.add(new ListItem("更新多条主唤醒词-小刚/乐乐", "updateMain"));
        mData.add(new ListItem("删除多条主唤醒词-小刚/乐乐", "removeMain1"));
        mData.add(new ListItem("删除多条主唤醒词-小红/天天", "removeMain2"));
        mData.add(new ListItem("获取主唤醒词", "getMain"));
        mData.add(new ListItem("清除主唤醒词", "clearMainWakeupWord"));
        mData.add(new ListItem("更新副唤醒词-小乐", "updateMinorWakeupWord"));
        mData.add(new ListItem("获取副唤醒词", "getMinorWord"));
        mData.add(new ListItem("添加一条命令唤醒词-开始播放", "addCommandWakeupWord"));
        mData.add(new ListItem("添加多条命令唤醒词-开始播放/暂停播放", "addCommandWakeupWords"));
        mData.add(new ListItem("移除一条命令唤醒词-开始播放", "removeCommandWakeupWord"));
        mData.add(new ListItem("移除多条命令唤醒词-开始播放/暂停播放", "removeCommandWakeupWords"));
        mData.add(new ListItem("更新一条命令唤醒词-下一首", "updateCommandWakeupWord"));
        mData.add(new ListItem("更新多条命令唤醒词-下一首/上一首", "updateCommandWakeupWords"));
        mData.add(new ListItem("清空命令唤醒词", "clearCommandWakeupWord"));
        mData.add(new ListItem("添加一条打断唤醒词-打断", "addShortcutWakeupWord"));
        mData.add(new ListItem("添加多条打断唤醒词-打断/停止", "addShortcutWakeupWords"));
        mData.add(new ListItem("移除一条打断唤醒词-打断", "removeShortcutWakeupWord"));
        mData.add(new ListItem("移除多条打断唤醒词-打断/停止", "removeShortcutWakeupWords"));
        mData.add(new ListItem("更新一条打断唤醒词-中断", "updateShortcutWakeupWord"));
        mData.add(new ListItem("更新多条打断唤醒词-中断/暂停", "updateShortcutWakeupWords"));
        mData.add(new ListItem("清空打断唤醒词", "clearShortCutWakeupWord"));
        mData.add(new ListItem("添加多条快速唤醒词-导航去/我想听", "addQuickStartWords"));
        mData.add(new ListItem("删除一条快速唤醒词-我想听", "removeQuickStartWord"));
        mData.add(new ListItem("删除多条快速唤醒词-导航去/我想听", "removeQuickStartWords"));
        mData.add(new ListItem("更新多条快速唤醒词-导航到", "updateQuickStartWords"));
        mData.add(new ListItem("清空快速唤醒词", "clearQuickStartWords"));
        mData.add(new ListItem("获取主唤醒词汉字", "getWakeupWords"));
        mData.add(new ListItem("获取当前oneshot状态", "getOneshotState"));
        mData.add(new ListItem("设置自定义欢迎语", "setWakeupCallback"));
        mData.add(new ListItem("修改唤醒增强角度", "setWakeupDoa"));
        mData.add(new ListItem("修改驾驶模式", "setWakeupMode"));
        mData.add(new ListItem("设置是唤醒模块的模式(信号处理不变)", "setWakeupSwitch"));
        mData.add(new ListItem("更新唤醒词,合并请求", "updateWakeupWords"));
        mData.add(new ListItem("开启oneshot", "enableOneShot"));
        mData.add(new ListItem("关闭oneshot", "disableOneShot"));
        mData.add(new ListItem("是否允许识别过程中响应唤醒", "enableWakeupWhenAsr"));
        mData.add(new ListItem("设置快捷命令唤醒词可唤醒", "enableCommandWakeupWord"));
        mData.add(new ListItem("设置快捷命令唤醒词不可唤醒", "disableCommandWakeupWord"));
        mData.add(new ListItem("设置主唤醒词可唤醒", "enableMainWakeupWord"));
        mData.add(new ListItem("设置主唤醒词不可唤醒", "disableMainWakeupWord"));
        mData.add(new ListItem("设置副唤醒词可唤醒", "enableMinorWakeupWord"));
        mData.add(new ListItem("设置副唤醒词不可唤醒", "disableMinorWakeupWord"));
        mData.add(new ListItem("设置打断唤醒词可唤醒", "enableShortCutWakeupWord"));
        mData.add(new ListItem("设置打断唤醒词不可唤醒", "disableShortCutWakeupWord"));
        mData.add(new ListItem("设置Beamforming监听器","setBfListener"));
        mData.add(new ListItem("设置Voip监听器","setVoipListener"));
        mData.add(new ListItem("获取当前唤醒内核的版本号","getWakeupVersion"));
        mData.add(new ListItem("就近唤醒-设置major字段","setMajor"));
        mData.add(new ListItem("就近唤醒-开启","enableNearWakeup"));
        mData.add(new ListItem("就近唤醒-关闭","disableNearWakeup"));
    }

    public void enableWakeup() {
        // 开启语音唤醒
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableWakeup();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableWakeup() {
        // 关闭语音唤醒
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableWakeup();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addMain() {
        try {
            WakeupWord minorWord = new WakeupWord()
                    .setPinyin("ni hao xiao ming")
                    .setWord("你好小明")
                    .setThreshold("0.25")
                    .addGreeting("小明又回来了");
            DDS.getInstance().getAgent().getWakeupEngine().addMainWakeupWord(minorWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addMain1() {
        try {
            WakeupWord minorWord = new WakeupWord()
                    .setPinyin("ni hao xiao mei")
                    .setWord("你好小美")
                    .setThreshold("0.15")
                    .addGreeting("小美回来了");
            DDS.getInstance().getAgent().getWakeupEngine().addMainWakeupWord(minorWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void removeMainWakeupWord() {
        try {
            WakeupWord minorWord = new WakeupWord()
                    .setPinyin("ni hao xiao ming")
                    .setWord("你好小明");
            DDS.getInstance().getAgent().getWakeupEngine().removeMainWakeupWord(minorWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addMainWakeupWords() {
        try {
            //实时添加主唤醒词-添加多条主唤醒词(最新支持)
            WakeupWord mainWord = new WakeupWord()
                    .setPinyin("ni hao xiao hong")
                    .setWord("你好小红")
                    .addGreeting("小红在")
                    .setThreshold("0.25");
            WakeupWord mainWord2 = new WakeupWord()
                    .setPinyin("ni hao tian tian")
                    .setWord("你好天天")
                    .addGreeting("天天在")
                    .setThreshold("0.15");
            List<WakeupWord> mainWordList = new ArrayList<>();
            mainWordList.add(mainWord);
            mainWordList.add(mainWord2);
            DDS.getInstance().getAgent().getWakeupEngine().addMainWakeupWords(mainWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateMain() {
        WakeupWord mainWord = new WakeupWord()
                .setPinyin("ni hao xiao gang")
                .setWord("你好小刚").setThreshold("0.15")
                .addGreeting("小刚在");
        WakeupWord mainWord2 = new WakeupWord()
                .setPinyin("ni hao le le")
                .setWord("你好乐乐")
                .setThreshold("0.25")
                .addGreeting("乐乐在");
        List<WakeupWord> mainWordList = new ArrayList<>();
        mainWordList.add(mainWord);
        mainWordList.add(mainWord2);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateMainWakeupWords(mainWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void removeMain1() {
        try {
            WakeupWord mainWord1 = new WakeupWord()
                    .setWord("你好小刚")
                    .setPinyin("ni hao xiao gang");
            WakeupWord mainWord2 = new WakeupWord()
                    .setWord("你好乐乐")
                    .setPinyin("ni hao le le");
            List<WakeupWord> mainWordList1 = new ArrayList<>();
            mainWordList1.add(mainWord1);
            mainWordList1.add(mainWord2);
            DDS.getInstance().getAgent().getWakeupEngine().removeMainWakeupWords(mainWordList1);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void removeMain2() {
        try {
            WakeupWord mainWord3 = new WakeupWord()
                    .setWord("你好小红")
                    .setPinyin("ni hao xiao hong");
            WakeupWord mainWord4 = new WakeupWord()
                    .setWord("你好天天")
                    .setPinyin("ni hao tian tian");
            List<WakeupWord> mainWordList = new ArrayList<>();
            mainWordList.add(mainWord3);
            mainWordList.add(mainWord4);
            DDS.getInstance().getAgent().getWakeupEngine().removeMainWakeupWords(mainWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getMain() {
        try {
            List<WakeupWord> wordList = DDS.getInstance().getAgent().getWakeupEngine().getMainWakeupWords();
            Log.d(TAG, "wordList = " + wordList.toString());
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateMinorWakeupWord() {
        // 实时更新副唤醒词(最新支持)
        WakeupWord minorWord = new WakeupWord()
                .setPinyin("ni hao xiao le")
                .setWord("你好小乐")
                .setThreshold("0.25")
                .addGreeting("小乐在");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateMinorWakeupWord(minorWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getMinorWord() {
        //获取当前的副唤醒词
        try {
            String minorWakeupWord = DDS.getInstance().getAgent().getWakeupEngine().getMinorWakeupWord();
            Log.d(TAG, "minorWakeupWord: " + minorWakeupWord);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addCommandWakeupWord() {
        // 实时添加命令唤醒词-添加一条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setPinyin("kai shi bo fang")
                .setWord("开始播放")
                .setThreshold("0.18")
                .addGreeting("好的请稍后")
                .setAction("sys.play");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addCommandWakeupWord(commandWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void removeCommandWakeupWord() {

        // 实时移除命令唤醒词-移除一条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setWord("开始播放");
//                .setPinyin("kai shi bo fang");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeCommandWakeupWord(commandWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void removeCommandWakeupWords() {

        // 实时移除命令唤醒词-移除多条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setWord("开始播放");
//                .setPinyin("kai shi bo fang");
        WakeupWord commandWord1 = new WakeupWord()
                .setWord("暂停播放");
//                .setPinyin("zan ting bo fang");
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeCommandWakeupWords(commandList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addCommandWakeupWords() {

        // 实时添加命令唤醒词-添加多条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setPinyin("kai shi bo fang")
                .setWord("开始播放")
                .setThreshold("0.18")
                .addGreeting("好的，开始播放")
                .setAction("sys.play");
        WakeupWord commandWord1 = new WakeupWord()
                .setPinyin("zan ting bo fang")
                .setWord("暂停播放")
                .addGreeting("好的，暂停播放")
                .setThreshold("0.20")
                .setAction("sys.pause");
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addCommandWakeupWords(commandList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void updateCommandWakeupWord() {

        // 实时更新命令唤醒词-更新一条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setPinyin("xia yi shou")
                .setWord("下一首")
                .setThreshold("0.18")
                .addGreeting("播放下一首")
                .setAction("sys.next");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateCommandWakeupWord(commandWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void updateCommandWakeupWords() {

        // 实时更新命令唤醒词-更新多条命令唤醒词(最新支持)
        WakeupWord commandWord = new WakeupWord()
                .setPinyin("xia yi shou")
                .setWord("下一首")
                .setThreshold("0.18")
                .addGreeting("播放下一首")
                .setAction("sys.next");
        WakeupWord commandWord1 = new WakeupWord()
                .setPinyin("shang yi shou")
                .setWord("上一首")
                .addGreeting("播放上一首")
                .setThreshold("0.20")
                .setAction("sys.last");
        List<WakeupWord> commandList = new ArrayList<>();
        commandList.add(commandWord);
        commandList.add(commandWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateCommandWakeupWords(commandList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void clearCommandWakeupWord() {
        //清空当前设置的命令唤醒词
        try {
            DDS.getInstance().getAgent().getWakeupEngine().clearCommandWakeupWord();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addShortcutWakeupWord() {
        //实时添加打断唤醒词-添加一条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setPinyin("da duan")
                .setWord("打断")
                .setThreshold("0.18");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addShortcutWakeupWord(fixWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addShortcutWakeupWords() {

        //实时添加打断唤醒词-添加多条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setPinyin("da duan")
                .setWord("打断")
                .setThreshold("0.18");
        WakeupWord fixWord1 = new WakeupWord()
                .setPinyin("ting zhi")
                .setWord("停止")
                .setThreshold("0.16");
        List<WakeupWord> fixList = new ArrayList<>();
        fixList.add(fixWord);
        fixList.add(fixWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addShortcutWakeupWords(fixList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void removeShortcutWakeupWord() {

        //实时移除打断唤醒词-移除一条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setWord("打断");
//                .setPinyin("da duan");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeShortcutWakeupWord(fixWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void removeShortcutWakeupWords() {

        //实时移除打断唤醒词-移除多条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setWord("打断");
//                .setPinyin("da duan");
        WakeupWord fixWord1 = new WakeupWord()
                .setWord("停止");
//                .setPinyin("ting zhi");
        List<WakeupWord> fixList = new ArrayList<>();
        fixList.add(fixWord);
        fixList.add(fixWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeShortcutWakeupWords(fixList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateShortcutWakeupWord() {

        //实时更新打断唤醒词-更新一条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setPinyin("zhong duan")
                .setWord("中断")
                .setThreshold("0.18");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateShortcutWakeupWord(fixWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateShortcutWakeupWords() {

        //实时更新打断唤醒词-更新多条打断唤醒词(最新支持)
        WakeupWord fixWord = new WakeupWord()
                .setPinyin("zhong duan")
                .setWord("中断")
                .setThreshold("0.18");
        WakeupWord fixWord1 = new WakeupWord()
                .setPinyin("zan ting")
                .setWord("暂停")
                .setThreshold("0.16");
        List<WakeupWord> fixList = new ArrayList<>();
        fixList.add(fixWord);
        fixList.add(fixWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateShortcutWakeupWords(fixList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void clearShortCutWakeupWord() {

        //清空当前设置的打断唤醒词
        try {
            DDS.getInstance().getAgent().getWakeupEngine().clearShortCutWakeupWord();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //1.oneshot 2.全双工模式下需设置config.addConfig(DDSConfig.K_USE_VAD_IN_FULLDUPLEX,"true");
    public void addQuickStartWords() {

        WakeupWord quickStartWord = new WakeupWord()
                .setPinyin("dao hang qu")
                .setWord("导航去")
                .setThreshold("0.15");
        WakeupWord quickStartWord1 = new WakeupWord()
                .setPinyin("wo xiang ting")
                .setWord("我想听")
                .setThreshold("0.15");
        List<WakeupWord> quickStartWordList = new ArrayList<>();
        quickStartWordList.add(quickStartWord);
        quickStartWordList.add(quickStartWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addQuickStartWords(quickStartWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }


    public void removeQuickStartWord() {

        //实时移除一条QuickStart词
        WakeupWord quickStartWord = new WakeupWord()
                .setWord("我想听");
//                .setPinyin("wo xiang ting");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeQuickStartWord(quickStartWord);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void removeQuickStartWords() {

        WakeupWord quickStartWord = new WakeupWord()
                .setWord("导航去");
//                .setPinyin("dao hang qu");
        WakeupWord quickStartWord1 = new WakeupWord()
                .setWord("我想听");
//                .setPinyin("wo xiang ting");

        List<WakeupWord> quickStartWordList = new ArrayList<>();
        quickStartWordList.add(quickStartWord);
        quickStartWordList.add(quickStartWord1);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().removeQuickStartWords(quickStartWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void updateQuickStartWords() {
        //更新多条QuickStart词的接口，覆盖原有的QuickStart词
        WakeupWord quickStartWord = new WakeupWord()
                .setPinyin("dao hang dao")
                .setWord("导航到")
                .setThreshold("0.15");
        List<WakeupWord> quickStartWordList = new ArrayList<>();
        quickStartWordList.add(quickStartWord);
        try {
            DDS.getInstance().getAgent().getWakeupEngine().updateQuickStartWords(quickStartWordList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearQuickStartWords() {

        //清空QuickStart词
        try {
            DDS.getInstance().getAgent().getWakeupEngine().clearQuickStartWords();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void enableShortCutWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableShortCutWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableShortCutWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableShortCutWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableMinorWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableMinorWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableMinorWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableMinorWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableMainWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableMainWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableMainWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableMainWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableCommandWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableCommandWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableCommandWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableCommandWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearMainWakeupWord() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().clearMainWakeupWord();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableWakeupWhenAsr() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableWakeupWhenAsr(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableOneShot() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableOneShot();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableOneShot() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().disableOneShot();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateWakeupWords() {
        try {
            List<WakeupWordIntent> wakeupWordIntentList = new ArrayList<>();
            // 添加主唤醒词
            WakeupWordIntent wakeupWordIntent0 = new WakeupWordIntent(WakeupType.ADD_MAIN)
                    .setPinyin("ni hao xiao le")
                    .setWord("你好小乐")
                    .setThreshold("0.16")
                    .addGreeting("小乐来了");

            WakeupWordIntent wakeupWordIntent1 = new WakeupWordIntent(WakeupType.ADD_COMMAND)
                    .setPinyin("kai shi bo fang")
                    .setWord("开始播放")
                    .setThreshold("0.18")
                    .addGreeting("好的，开始播放")
                    .setAction("sys.play");

            WakeupWordIntent wakeupWordIntent2 = new WakeupWordIntent(WakeupType.ADD_SHORTCUT)
                    .setPinyin("da duan")
                    .setWord("打断")
                    .setThreshold("0.18");

            wakeupWordIntentList.add(wakeupWordIntent0);
            wakeupWordIntentList.add(wakeupWordIntent1);
            wakeupWordIntentList.add(wakeupWordIntent2);

            DDS.getInstance().getAgent().getWakeupEngine().updateWakeupWords(wakeupWordIntentList);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setWakeupCallback() {
        DDS.getInstance().getAgent().getWakeupEngine().setWakeupCallback(new WakeupCallback() {
            @Override
            public JSONObject onWakeup(JSONObject jsonObject) {
                Log.d(TAG, "onWakeupResult = " + jsonObject.toString());

                try {
                    JSONObject resultObj = new JSONObject().put("greeting", "这是自定义欢迎语");
                    return resultObj;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }

    public void setWakeupSwitch() {
        // 在信号处理不变的前提下是否关闭唤醒 true-开启唤醒 ；false-关闭唤醒
        try {
            DDS.getInstance().getAgent().getWakeupEngine().setWakeupSwitch(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setWakeupMode() {
        // 车载模式下设置驾驶模式
        // 驾驶模式, 0:定位模式, 1：主驾模式, 2: 副驾模式  3：全车模式
        try {
            DDS.getInstance().getAgent().getWakeupEngine().setWakeupMode(1);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setWakeupDoa() {
        // 修改唤醒增强角度, 环麦线麦中使用
        try {
            DDS.getInstance().getAgent().getWakeupEngine().setWakeupDoa(90);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getWakeupWords() {
        try {
            String[] wakeupWords = DDS.getInstance().getAgent().getWakeupEngine().getWakeupWords();
            if (wakeupWords != null) {
                for (String wakeupWord : wakeupWords) {
                    Log.d(TAG, "wakeupWord = " + wakeupWord);
                }
            }
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getOneshotState() {
        try {
            boolean oneshotState = DDS.getInstance().getAgent().getWakeupEngine().getOneshotState();
            Log.d(TAG, "oneshotState = " + oneshotState);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setBfListener() {
        BfListener listener =  new BfListener() {
            @Override
            public void onBeamforming(byte[] bytes) {
                Log.d(TAG,"bytes:"+bytes);
            }
        };

        try {
            DDS.getInstance().getAgent().getWakeupEngine().setBfListener(listener);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setVoipListener() {
        try {
            VoipListener listener = new VoipListener() {
                @Override
                public void onVoip(byte[] pcm) {
                    Log.d(TAG, "onVoip = " + pcm.length);
                }
            };
            DDS.getInstance().getAgent().getWakeupEngine().setVoipListener(listener);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getWakeupVersion() {
        try {
            String version = DDS.getInstance().getAgent().getWakeupEngine().getWakeupVersion();
            Log.d(TAG, "getWakeupVersion: "+version);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setMajor() {
        //在设置唤醒词的时候设置major字段
        WakeupWord minorWord = new WakeupWord()
                .setPinyin("ni hao xiao chi")
                .setWord("你好小驰")
                .setThreshold("0.25")
                .setMajor("1")
                .addGreeting("小驰回来了");
        try {
            DDS.getInstance().getAgent().getWakeupEngine().addMainWakeupWord(minorWord);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableNearWakeup() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableNearWakeup(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableNearWakeup() {
        try {
            DDS.getInstance().getAgent().getWakeupEngine().enableNearWakeup(false);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

}
