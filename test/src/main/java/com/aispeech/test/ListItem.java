package com.aispeech.test;

public class ListItem<T> {
    private String title;
    private Class<T> klass;
    private String method;

    public ListItem(String title, Class<T> klass) {
        this.title = title;
        this.klass = klass;
    }

    public ListItem(String title, String method) {
        this.title = title;
        this.method = method;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<T> getKlass() {
        return klass;
    }

    public void setKlass(Class<T> klass) {
        this.klass = klass;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
