package com.aispeech.test.agent;

import android.os.SystemClock;
import android.util.Log;

import com.aispeech.dui.BusClient;
import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.DDSMode;
import com.aispeech.dui.dds.agent.DMCallback;
import com.aispeech.dui.dds.agent.SkillIntent;
import com.aispeech.dui.dds.agent.wakeup.cb.VoipListener;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

public class AgentTestActivity extends BaseTestActivity {

    @Override
    protected void initData() {
        mData.add(new ListItem<>("文本请求", "sendText"));
        mData.add(new ListItem<>("开启对话", "startDialog"));
        mData.add(new ListItem<>("终止对话", "stopDialog"));
        mData.add(new ListItem<>("开启对话播报文本", "startDialogWithSpeakText"));
        mData.add(new ListItem<>("终止对话播报文本", "stopDialogWithSpeakText"));
        mData.add(new ListItem<>("点击唤醒/停止识别/打断播报avatarClick", "avatarClick"));
        mData.add(new ListItem<>("avatarClick附带欢迎语", "avatarClickWithSpeakText"));
        mData.add(new ListItem<>("avatarPress", "avatarPress"));
        mData.add(new ListItem<>("avatarRelease", "avatarRelease"));
        mData.add(new ListItem<>("静音模式", "setDDSModeSilence"));
        mData.add(new ListItem<>("正常模式", "setDDSModeNormal"));
        mData.add(new ListItem<>("近场模式", "setDDSModeNear"));
        mData.add(new ListItem<>("远场模式", "setDDSModeFar"));
        mData.add(new ListItem<>("触发指定意图", "triggerIntent1"));
        mData.add(new ListItem<>("根据skillid触发意图", "triggerIntent2"));
        mData.add(new ListItem<>("根据skillid触发意图2", "triggerIntent3"));
        mData.add(new ListItem<>("根据skillid触发意图3", "triggerIntent4"));
        mData.add(new ListItem<>("获取deviceName-TestDeviceName0001","getDeviceName"));
        mData.add(new ListItem("多轮对话中强制设置为首轮","endSkillInDialog"));
        mData.add(new ListItem<>("多轮对话中强制设置为首轮1","endSkillInDialog1"));
        mData.add(new ListItem<>("对话结果支持内容修改","setDMCallback"));
        mData.add(new ListItem<>("开启voip功能","setEnableVoip"));
        mData.add(new ListItem<>("关闭voip功能","setDisableVoip"));
        mData.add(new ListItem<>("打开tip提示音","openTip"));
        mData.add(new ListItem<>("关闭tip提示音","closeTip"));
    }

    public void sendText() {
        try {
            DDS.getInstance().getAgent().sendText("苏州天气");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void startDialog() {

        // 开启对话。如果当前已经在对话中，则重新开启新一轮对话。
        try {
            DDS.getInstance().getAgent().startDialog();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void stopDialog() {

        // 关闭对话
        try {
            DDS.getInstance().getAgent().stopDialog();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void startDialogWithSpeakText() {
        //开启对话
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("speakText","开启对话");
            // 开启对话。如果当前已经在对话中，则重新开启新一轮对话。
            try {
                DDS.getInstance().getAgent().startDialog(jsonObject);
                toMain();
            } catch (DDSNotInitCompleteException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void stopDialogWithSpeakText() {

        //结束对话
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("speakText","结束对话");
            // 开启对话。如果当前已经在对话中，则重新开启新一轮对话。
            try {
                DDS.getInstance().getAgent().stopDialog(jsonObject);
                toMain();
            } catch (DDSNotInitCompleteException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void avatarClick() {

        // 点击唤醒/停止识别/打断播报 操作接口
        try {
            DDS.getInstance().getAgent().avatarClick();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void avatarClickWithSpeakText() {

        // 点击唤醒/停止识别/打断播报 操作接口 , 并附带一则欢迎语，当此次是唤醒时，播报这则欢迎语
        try {
            DDS.getInstance().getAgent().avatarClick("有什么可以帮你");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void avatarPress() {
        // 按下按键接口(调用此接口需关闭VAD,VAD在创建产品时可配置)
        try {
            DDS.getInstance().getAgent().avatarPress();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void avatarRelease() {
        // 释放按键接口(调用此接口需关闭VAD，VAD在创建产品时可配置)
        try {
            DDS.getInstance().getAgent().avatarRelease();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setDDSModeSilence() {

        try {
            DDS.getInstance().getAgent().setDDSMode(DDSMode.TTS_SILENCE);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setDDSModeNormal() {

        try {
            DDS.getInstance().getAgent().setDDSMode(DDSMode.TTS_NORMAL); //默认值。
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setDDSModeNear() {

        ////在近场模式下，将加载产品配置页中的近场配置
        try {
            DDS.getInstance().getAgent().setDDSMode(DDSMode.PICKUP_NEAR);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setDDSModeFar(){

        ////在远场模式下，将加载产品配置页中的远场配置
        try {
            DDS.getInstance().getAgent().setDDSMode(DDSMode.PICKUP_FAR);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void triggerIntent1() {

        //跳过识别和语义，直接进入指定的意图对话。即：DDS主动向用户发起一轮对话。
        try {
            DDS.getInstance().getAgent().triggerIntent("本地技能test1", "本地技能测试", "智能家居", new JSONObject().put("家居", "台灯").toString());
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void triggerIntent2() {

        SkillIntent skillIntent = null;
        try {
            skillIntent = new SkillIntent("2019082800000473", "本地技能测试", "智能家居", new JSONObject().put("家居", "台灯").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG,"skillIntent:"+skillIntent);

        try {
            DDS.getInstance().getAgent().triggerIntent(skillIntent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void triggerIntent3() {
        try {
            SkillIntent skillIntent = new SkillIntent();
            skillIntent.setSkillId("2019082800000473");
            skillIntent.setTaskName("本地技能测试");
            skillIntent.setIntentName("智能家居");
            skillIntent.setInput("input");

            SkillIntent.ItemSlots itemSlots = new SkillIntent.ItemSlots("家居", "台灯");
            itemSlots.setRawvalue("rawValue");
            itemSlots.setRawpinyin("rawPinyin");
            skillIntent.addItemSlots(itemSlots);

            Log.e(TAG, "skillIntent = " + skillIntent.toJson().toString());

            DDS.getInstance().getAgent().triggerIntent(skillIntent);
//            toMain();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void triggerIntent4() {

        SkillIntent skillIntent = null;
        try {
            skillIntent = new SkillIntent("2019082800000473", "本地技能测试", "智能家居", new JSONObject().put("家居", "台灯").toString());
            skillIntent.setCustom(new JSONObject().put("phone", "10086")); // 添加自定义参数, 此参数会在sys.dialog.start消息中返回给用户
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG,"skillIntent:"+skillIntent);

        try {
            DDS.getInstance().getAgent().triggerIntent(skillIntent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    //需要配置config.addConfig(DDSConfig.K_DEVICE_NAME, "TestDeviceName0001");
    public void getDeviceName() {

        BusClient.RPCResult rpcResult = DDS.getInstance().getAgent().getBusClient().call("/local_auth/device/info");
        String deviceInfo = rpcResult.getStringResult();
        Log.e(TAG,"deviceInfo:"+deviceInfo);
    }

    public void endSkillInDialog() {
        try {
            DDS.getInstance().getAgent().endSkillInDialog();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void endSkillInDialog1() {
        // 多模态事件同步,该接口用于客户端给对话中控发送一个事件(可替代endSkillInDialog接口)
        JSONObject obj = new JSONObject();
        try {
            obj.put("endSkill", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            DDS.getInstance().getAgent().updateDispatchEvent(obj);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setDMCallback() {
        //对话结果支持内容修改
        DDS.getInstance().getAgent().setDMCallback(new DMCallback() {
            @Override
            public JSONObject onDMResult(JSONObject dmResult) {
                try {
                    dmResult.getJSONObject("dm").put("nlg", "哈哈哈");
                    dmResult.getJSONObject("dm").getJSONObject("speak").put("text",
                            "12345");
                    Log.d("xlg","dmResult:"+dmResult);
                } catch (JSONException e) {
                    e.printStackTrace();
                }finally {
                    return dmResult;
                }
            }
        });
    }

    public void openTip() {

        try {
            DDS.getInstance().getAgent().openTip();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void closeTip() {

        try {
            DDS.getInstance().getAgent().closeTip();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setEnableVoip() {

        VoipListener listener = new VoipListener() {
            @Override
            public void onVoip(byte[] pcm) {
                Log.d(TAG, "onVoip = " + pcm.length);
            }
        };
        try {
            DDS.getInstance().getAgent().getWakeupEngine().setVoipListener(listener);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

        try {
            DDS.getInstance().getAgent().setEnableVoip(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                feed();
            }
        }).start();

    }

    public void setDisableVoip() {

        try {
            DDS.getInstance().getAgent().setEnableVoip(false);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    private void feed() {
        try {
            InputStream is = this.getAssets().open("line4.pcm");
            byte[] buffer = new byte[3200 * 4];
            int length;
            while ((length = is.read(buffer)) != -1) {
                byte[] temp = new byte[length];
                System.arraycopy(buffer, 0, temp, 0, length);
                DDS.getInstance().getAgent().feedPcm(temp);
                SystemClock.sleep(100);
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
