package com.aispeech.test;

import android.app.Activity;
import android.content.Intent;

public class TestUtil {
    private static Class mHomeClazz;

    public static void setHomeAcitivty(Class clazz) {
        mHomeClazz = clazz;
    }

    public static void toHome(Activity activity) {
        if (activity != null && mHomeClazz != null) {
            activity.startActivity(new Intent(activity, mHomeClazz));
        }
    }


}
