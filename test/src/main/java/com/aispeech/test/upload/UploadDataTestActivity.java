package com.aispeech.test.upload;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.ContextIntent;
import com.aispeech.dui.dds.agent.PhraseHintsIntent;
import com.aispeech.dui.dds.agent.VocabIntent;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

import java.util.Arrays;

public class UploadDataTestActivity extends BaseTestActivity {
    @Override
    protected void initData() {
        mData.add(new ListItem<>("更新设备状态-蓝牙开关-open", "updateProductContext1"));
        mData.add(new ListItem<>("更新设备状态-蓝牙开关-closed", "updateProductContext2"));
        mData.add(new ListItem<>("更新技能配置-平台类型-Android", "updateSkillContext1"));
        mData.add(new ListItem<>("更新技能配置-平台类型-iOS", "updateSkillContext2"));
        mData.add(new ListItem<>("获取产品的配置信息", "getProductContext"));
        mData.add(new ListItem<>("获取技能的配置信息", "getSkillContext"));
        mData.add(new ListItem<>("更新热词-章大樵/媛保保", "updatePhraseHints"));
        mData.add(new ListItem<>("清空热词", "clearPhraseHints"));
        mData.add(new ListItem<>("本地-添加词条-洗衣机/蓝牙音响/小乐音响", "addVocab"));
        mData.add(new ListItem<>("本地-清空并添加词条-电脑/风扇", "clearAndInsertVocab"));
        mData.add(new ListItem<>("本地-删除词条-电视", "removeVocab"));
        mData.add(new ListItem<>("本地-删除词条-电脑", "removeVocab1"));
        mData.add(new ListItem<>("本地-删除词条-风扇", "removeVocab2"));
        mData.add(new ListItem<>("清空所有词条", "clearAllVocab"));
        mData.add(new ListItem<>("云端-添加词条-小红/小明", "addVocabCloud"));
        mData.add(new ListItem<>("云端-清空并添加词条-龙龙", "clearAndInsertVocabCloud"));
        mData.add(new ListItem<>("云端-删除词条-小红/小明", "removeVocabCloud"));
        mData.add(new ListItem<>("云端-清空所有词条", "clearAllVocabCloud"));
        mData.add(new ListItem<>("批量更新词库", "updateVocabs"));
    }

    //更新结果可以通过sys.upload.result消息来获取
    public void updateProductContext1() {

        //数据上传
        ContextIntent intent2 = new ContextIntent("status","{\"platform\":\"open\"}");
        try {
            DDS.getInstance().getAgent().updateProductContext(intent2);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //更新结果可以通过sys.upload.result消息来获取
    public void updateProductContext2() {

        //数据上传
        ContextIntent intent2 = new ContextIntent("status","{\"platform\":\"closed\"}");
        try {
            DDS.getInstance().getAgent().updateProductContext(intent2);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateSkillContext1() {

        ContextIntent intent2 = new ContextIntent("status","{\"platform\":\"Android\"}");
        intent2.setSkillId("2019062000000368");
        try {
            DDS.getInstance().getAgent().updateSkillContext(intent2);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateSkillContext2() {

        ContextIntent intent2 = new ContextIntent("status","{\"platform\":\"iOS\"}");
        intent2.setSkillId("2019062000000368");
        try {
            DDS.getInstance().getAgent().updateSkillContext(intent2);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getProductContext() {
        try {
            DDS.getInstance().getAgent().getProductContext("status");
            Log.e(TAG,"productContext:"+DDS.getInstance().getAgent().getProductContext("status"));
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getSkillContext() {
        try {
            DDS.getInstance().getAgent().getSkillContext("2019062000000368", "status");
            Log.d(TAG,"skillContext:"+DDS.getInstance().getAgent().getSkillContext("2019062000000368", "status"));
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updatePhraseHints() {
        //全双工 只在airobot下支持
        PhraseHintsIntent intent = new PhraseHintsIntent();
        intent.setType("vocab");
        intent.setName("sys.联系人");
        intent.addData("章大樵");
        intent.addData("媛保保");
        try {
            DDS.getInstance().getAgent().updatePhraseHints(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearPhraseHints() {
        try {
            DDS.getInstance().getAgent().updatePhraseHints();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void addVocabCloud(){
        try {
            final String reqId = DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("伙伴名")
                            .setAction(VocabIntent.ACTION_INSERT)
                            .setContents(Arrays.asList("小明", "小红"))
            );
            Log.e("xlg","reqId:"+reqId);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

//        try {
//            final String reqId = DDS.getInstance().getAgent().updateVocabs(
//                    new VocabIntent()
//                            .setName("伙伴名")
//                            .setAction(VocabIntent.ACTION_INSERT)
//                            .addContent("小明")
//                            .addContent("小红")
//            );
//            Log.e("xlg","reqId:"+reqId);
//        } catch (DDSNotInitCompleteException e) {
//            e.printStackTrace();
//        }


    }

    public void clearAndInsertVocabCloud() {

        //清空之前上传到该词库的所有词条，然后添加词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("伙伴名")
                            .setAction(VocabIntent.ACTION_CLEAR_AND_INSERT)
                            .setContents(Arrays.asList("龙龙"))
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void removeVocabCloud() {
        //删除该词库的词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("伙伴名")
                            .setAction(VocabIntent.ACTION_REMOVE)
                            .addContent("小红")
                            .addContent("小明")
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearAllVocabCloud() {
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("伙伴名")
                            .setAction(VocabIntent.ACTION_CLEAR_ALL)
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }



    public void addVocab() {
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_INSERT)
                            .setContents(Arrays.asList("洗衣机", "音响:蓝牙音箱,小乐音箱"))
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearAndInsertVocab() {

        //清空之前上传到该词库的所有词条，然后添加词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_CLEAR_AND_INSERT)
                            .setContents(Arrays.asList("电脑", "风扇"))
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


    public void removeVocab() {
        //删除该词库的词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_REMOVE)
                            .addContent("电视")
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }


    public void removeVocab1() {
        //删除该词库的词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_REMOVE)
                            .addContent("电脑")
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }


    public void removeVocab2() {
        //删除该词库的词条
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_REMOVE)
                            .addContent("风扇")
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void clearAllVocab() {
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_CLEAR_ALL)
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateVocabs() {
        //批量更新词库
        try {
            DDS.getInstance().getAgent().updateVocabs(
                    //向指定词库中添加词条
                    new VocabIntent()
                            .setName("sys.联系人")
                            .setAction(VocabIntent.ACTION_INSERT)
                            .addContent("董芳芳")
                            .addContent("王强"),
                    //清空之前上传到该词库的所有词条，然后添加词条
                    new VocabIntent()
                            .setName("sys.地址")
                            .setAction(VocabIntent.ACTION_CLEAR_AND_INSERT)
                            .setContents(Arrays.asList("帝都", "上海:魔都")),
                    //删除该词库的词条
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_CLEAR_AND_INSERT)
                            .addContent("风扇"),
                    //清空之前上传到该词库的所有词条
                    new VocabIntent()
                            .setName("家居")
                            .setAction(VocabIntent.ACTION_CLEAR_ALL)
            );
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }


}
