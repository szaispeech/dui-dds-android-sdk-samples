package com.aispeech.test.vad;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

public class VadTestActivity extends BaseTestActivity {
    @Override
    protected void initData() {
        mData.add(new ListItem<>("设置VAD后端停顿时间", "setVadPauseTime"));
        mData.add(new ListItem<>("获取VAD后端停顿时间", "getVadPauseTime"));
        mData.add(new ListItem<>("设置VAD前端静音检测超时时间", "setVadTimeout"));
        mData.add(new ListItem<>("获取VAD前端静音检测超时时间", "getVadTimeout"));
    }

    public void setVadPauseTime() {

        //设置VAD后端停顿时间
        try {
            DDS.getInstance().getAgent().getASREngine().setVadPauseTime(600);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getVadPauseTime() {

        //获取VAD后端停顿时间
        try {
            long vadPauseTime = DDS.getInstance().getAgent().getASREngine().getVadPauseTime();
            Log.d(TAG,"vadPauseTime:"+vadPauseTime);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setVadTimeout() {
        //设置VAD前端静音检测超时时间
        try {
            DDS.getInstance().getAgent().getASREngine().setVadTimeout(8008);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getVadTimeout() {

        //获取VAD前端静音检测超时时间
        try {
            long vadPauseTime = DDS.getInstance().getAgent().getASREngine().getVadTimeout();
            Log.d(TAG,"vadPauseTime:"+vadPauseTime);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }




}
