package com.aispeech.test.tts;

import android.media.AudioManager;
import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.tts.TTSEngine;
import com.aispeech.dui.dds.agent.tts.bean.CustomAudioBean;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

import java.util.ArrayList;
import java.util.List;

public class TTSTestActivity extends BaseTestActivity {
    @Override
    protected void initData() {
        mData.add(new ListItem<>("TTS事件回调", "setTTSListener"));
        mData.add(new ListItem<>("TTS事件回调-带进度条", "setListenerByProcess"));
        mData.add(new ListItem<>("语音播报-text-优先级1", "speakTextWithPriorityOne"));
        mData.add(new ListItem<>("语音播报-text-优先级2", "speakTextWithPriorityTwo"));
        mData.add(new ListItem<>("语音播报-text-优先级3", "speakTextWithPriorityThree"));
        mData.add(new ListItem<>("语音播报-SSML", "speakSSML"));
        mData.add(new ListItem<>("停止播报ttsId-10001", "shupWithTTSId"));
        mData.add(new ListItem<>("停止当前播报", "shupWithZero"));
        mData.add(new ListItem<>("停止所有播报", "shupWithNULL"));
        mData.add(new ListItem<>("设置TTS引擎为本地", "setLocal"));
        mData.add(new ListItem<>("设置TTS引擎为云端", "setCloud"));
        mData.add(new ListItem<>("设置发音人-zhilingf", "setSpeaker1"));
        mData.add(new ListItem<>("设置发音人-gdgm", "setSpeaker2"));
        mData.add(new ListItem<>("设置发音人-feyinf", "setSpeaker3"));
        mData.add(new ListItem<>("设置发音人-boy", "setSpeaker4"));
        mData.add(new ListItem<>("设置发音人-随机", "setSpeaker5"));
        mData.add(new ListItem<>("设置发音人-lzliafp", "setSpeaker6"));
        mData.add(new ListItem<>("设置发音人-自定义合成类型geyou", "setCustomSpeaker"));
        mData.add(new ListItem<>("设置语速-0.8", "setSpeed"));
        mData.add(new ListItem<>("设置语速-1.5", "setSpeed1"));
        mData.add(new ListItem<>("设置音量-10", "setVolume"));
        mData.add(new ListItem<>("设置音量-1", "setVolume1"));
        mData.add(new ListItem<>("设置音量-50", "setVolume2"));
        mData.add(new ListItem<>("设置TTS播报的通道", "setStreamType"));
        mData.add(new ListItem<>("设置TTS播报的通道1", "setStreamType1"));
        mData.add(new ListItem<>("设置TTS播报的通道2", "setStreamType2"));
        mData.add(new ListItem<>("设置TTS播报自定义录音", "setCustomAudio"));
        mData.add(new ListItem<>("设备抢焦点", "enableFocus"));
        mData.add(new ListItem<>("设备不抢焦点", "disableFocus"));
        mData.add(new ListItem<>("设置TTS人设", "setStyle"));
        mData.add(new ListItem<>("清除TTS人设", "removeStyle"));
        mData.add(new ListItem<>("获取合成音发音人", "getSpeaker"));
        mData.add(new ListItem<>("获取合成音语速", "getSpeed"));
        mData.add(new ListItem<>("获取合成音音量", "getVolume"));
        mData.add(new ListItem<>("获取当前自定义播报音频的列表", "getCustomAudio"));
        mData.add(new ListItem<>("获取当前TTS人设", "getStyle"));
        mData.add(new ListItem<>("设置TTS结束后延迟时间-2000", "setPlayAfterTime"));
        mData.add(new ListItem<>("设置TTS结束后延迟时间-0", "setPlayAfterTime1"));
        mData.add(new ListItem<>("打开音素返回", "enablePhoneme"));
        mData.add(new ListItem<>("关闭音素放回", "disablePhoneme"));
    }

    public void setTTSListener() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setListener(new TTSEngine.Callback() {
                /**
                 * 开始合成时的回调
                 * @param ttsId 当前TTS的id， 对话过程中的播报ttsid默认为0，通过speak接口调用的播报，ttsid由speak接口指定。
                 */
                @Override
                public void beginning(String ttsId) {
                    Log.d(TAG, "TTS开始播报 ttsId = " + ttsId);
                }

                /**
                 * 合成的音频数据的回调，可能会返回多次，data长度为0表示音频结束
                 * @param data 音频数据
                 */
                @Override
                public void received(byte[] data) {
                    Log.d(TAG, "收到音频，此方法会回调多次，直至data为0，音频结束 data = " + data.length);
                }

                /**
                 * TTS播报完成的回调
                 * @param status 播报结束的状态。
                 *               正常播报结束为0
                 *               播报中途被打断结束为1
                 */
                @Override
                public void end(String ttsId, int status) {
                    Log.d(TAG, "TTS播报结束 status = " + status + ", ttsId = " + ttsId);
                }

                /**
                 * 合成过程中出现错误的回调
                 * @param error 错误信息
                 */
                @Override
                public void error(String error) {
                    Log.d(TAG, "出现错误，" + error);
                }

                @Override
                public void phoneReturnReceived(String s) {

                }
            });
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setListenerByProcess() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setListenerByProcess(new TTSEngine.CallbackOptimize() {
                /**
                 * 开始合成时的回调
                 * @param ttsId 当前TTS的id， 对话过程中的播报ttsid默认为0，通过speak接口调用的播报，ttsid由speak接口指定。
                 */
                @Override
                public void beginning(String ttsId) {
                    Log.d(TAG, "TTS开始播报 ttsId = " + ttsId);
                }

                /**
                 * TTS播报完成的回调
                 * @param status 播报结束的状态。
                 *               正常播报结束为0
                 *               播报中途被打断结束为1
                 */
                @Override
                public void end(String ttsId, int status) {
                    Log.d(TAG, "TTS播报结束 status = " + status + ", ttsId = " + ttsId);
                }

                /**
                 * 合成过程中出现错误的回调
                 * @param error 错误信息
                 */
                @Override
                public void error(String error) {
                    Log.d(TAG, "出现错误，" + error);
                }

                @Override
                public void phoneReturnReceived(String s) {

                }

                /**
                 * 进度条
                 * @param ttsId  当前TTS的id， 对话过程中的播报ttsid默认为0，通过speak接口调用的播报，ttsid由speak接口指定。
                 * @param currentFrame  当前播放器已经播放的帧数
                 * @param totalFrame    当前tts总下载帧数
                 * @param isDataReady   数据是否下载完成
                 */
                @Override
                public void onSpeechProgress(String ttsId, int currentFrame, int totalFrame, boolean isDataReady) {
                    Log.d(TAG, "ttsId:" + ttsId + ", currentFrame:" + currentFrame + ", totalFrame:" + totalFrame + ", isDataReady:" + isDataReady);
                }
            });
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void speakTextWithPriorityOne() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().speak("hello, nice to meet you", 1, "10001", AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void speakTextWithPriorityTwo() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().speak("人工智能（Artificial Intelligence），英文缩写为AI。它是研究、开发用于模拟、延伸和扩展人的智能的理论、方法、技术及应用系统的一门新的技术科学。", 2, "10002", AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void speakTextWithPriorityThree() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().speak("用来研究人工智能的主要物质基础以及能够实现人工智能技术平台的机器就是计算机，人工智能的发展历史是和计算机科学技术的发展史联系在一起的。除了计算机科学以外，人工智能还涉及信息论、控制论、自动化、仿生学、生物学、心理学、数理逻辑、语言学、医学和哲学等多门学科。人工智能学科研究的主要内容包括：知识表示、自动推理和搜索方法、机器学习和知识获取、知识处理系统、自然语言理解、计算机视觉、智能机器人、自动程序设计等方面。", 3, "10003", AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void speakSSML() {
        //SSML播报
        String test1 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<speak>\n" +
                "    思必驰成立于2007年英国剑桥高新区，创始人均来自剑桥，2008年回国落户苏州。\n" +
                "    \n" +
                "    <prosody volume=\"x-loud\"> \n" +
                "        " +
                "是拥有人机对话技术，国际上极少数拥有自主产权、中英文综合语音技术（语音识别、语音合成、自然语言理解、智能交互决策、声纹识别、性别及年龄识别、情绪识别等）的公司之一,\n" +
                "    </prosody> \n" +
                "     \n" +
                "    <prosody rate=\"x-slow\">\n" +
                "    其语音识别、声纹识别、口语对话系统等技术曾经多次在美国国家标准局、美国国防部、国际研究机构评测中夺得冠军，\n" +
                "    </prosody>\n" +
                "    <prosody pitch=\"+5%\">\n" +
                "        代表了技术的国际前沿水平,\n" +
                "    </prosody>\n" +
                "    \n" +
                "    <prosody pitch=\"-10%\">\n" +
                "        被中国和英国政府评为高新技术企业。\n" +
                "    </prosody>\n" +
                "    \n" +
                "</speak>";

        try {
            DDS.getInstance().getAgent().getTTSEngine().speak(test1, 1, "ssml");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void shupWithTTSId() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().shutup("10001");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void shupWithZero() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().shutup("0");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void shupWithNULL() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().shutup("");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setLocal() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setMode(TTSEngine.LOCAL);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setCloud() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setMode(TTSEngine.CLOUD);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker1() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("zhilingfp");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker2() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("gdgm");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker3() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("feyinf");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker4() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("boy");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker5() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker(null);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeaker6() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("lzliafp");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setCustomSpeaker() {
        //设置发音人
        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeaker("geyou", "sdcard/geyoum_common_back_ce_local.v2.1.0.bin");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeed() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeed(0.8f);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setSpeed1() {

        try {
            DDS.getInstance().getAgent().getTTSEngine().setSpeed(1.5f);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setVolume() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setVolume(10);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setVolume1() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setVolume(1);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setVolume2() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setVolume(50);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setStreamType() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setStreamType(AudioManager.STREAM_RING);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setStreamType1() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setStreamType(AudioManager.STREAM_MUSIC);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setStreamType2() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().setStreamType(AudioManager.STREAM_ALARM);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setCustomAudio() {

        CustomAudioBean audioBean = new CustomAudioBean();
        audioBean.setName("我在，有什么可以帮你");
        String filePath = "/sdcard/nhxhua.mp3";
        Log.d(TAG, "filePath:" + filePath);
        audioBean.setPath(filePath);

        CustomAudioBean audioBean1 = new CustomAudioBean();
        audioBean1.setName("开始为您导航");
        String filePath1 = "/sdcard/xiaole.wav";
        Log.d(TAG, "filePath:" + filePath);
        audioBean1.setPath(filePath1);

        ArrayList customAudioList = new ArrayList();
        customAudioList.add(audioBean);
        customAudioList.add(audioBean1);

        try {
            DDS.getInstance().getAgent().getTTSEngine().setCustomAudio(customAudioList);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableFocus() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().enableFocus(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableFocus() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().enableFocus(false);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setStyle() {
        /**
         * 设置TTS人设
         * @param style 风格，humor:幽默;calm:沉稳;common:普通;简短:short;
         */
        try {
            DDS.getInstance().getAgent().getTTSEngine().setStyle("humor");
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void removeStyle() {
        try {
            DDS.getInstance().getAgent().getTTSEngine().removeStyle();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //返回String 当前使用的合成音类型,如："zhilingf",获取失败返回null
    public void getSpeaker() {
        try {
            String speaker = DDS.getInstance().getAgent().getTTSEngine().getSpeaker();
            Log.d(TAG, "speaker:" + speaker);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //返回float 当前合成音语速，返回值0.5-2.0，0.5语速最快，2.0语速最慢，获取失败返回 0
    public void getSpeed() {
        try {
            float speed = DDS.getInstance().getAgent().getTTSEngine().getSpeed();
            Log.d(TAG, "speed:" + speed);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //返回int 当前合成音音量,返回值1-100,获取失败返回 0
    public void getVolume() {
        try {
            int volume = DDS.getInstance().getAgent().getTTSEngine().getVolume();
            Log.d(TAG, "volume:" + volume);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //返回List<CustomAudioBean> 当前自定义播报音频的列表
    public void getCustomAudio() {
        try {
            List customAudio = DDS.getInstance().getAgent().getTTSEngine().getCustomAudio();
            Log.d(TAG, "customAudio:" + customAudio);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getStyle() {
        try {
            String style = DDS.getInstance().getAgent().getTTSEngine().getStyle();
            Log.d(TAG, "speaker:" + style);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setPlayAfterTime() {

        //v1.2.3.1
        try {
            DDS.getInstance().getAgent().getTTSEngine().setPlayAfterTime(2000);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void setPlayAfterTime1() {

        //v1.2.3.1
        try {
            DDS.getInstance().getAgent().getTTSEngine().setPlayAfterTime(0);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enablePhoneme() {
        //2.7.0.1
        try {
            DDS.getInstance().getAgent().getTTSEngine().setPhoneReturn(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disablePhoneme() {
        //2.7.0.1
        try {
            DDS.getInstance().getAgent().getTTSEngine().setPhoneReturn(false);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }
}
