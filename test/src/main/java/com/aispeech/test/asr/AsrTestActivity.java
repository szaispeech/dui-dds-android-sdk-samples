package com.aispeech.test.asr;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.ASREngine;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.AssetsUtil;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

public class AsrTestActivity extends BaseTestActivity {

    @Override
    protected void initData() {
        mData.add(new ListItem<>("开启识别", "startListening"));
        mData.add(new ListItem<>("停止识别", "stopListening"));
        mData.add(new ListItem<>("取消识别", "cancelListening"));
        mData.add(new ListItem<>("关闭实时回传音量", "disableVolume"));
        mData.add(new ListItem<>("开启实时回传音量", "enableVolume"));
        mData.add(new ListItem<>("更新云端识别模型-aihome", "updateAsrModel1"));
        mData.add(new ListItem<>("更新云端识别模型-airobot", "updateAsrModel2"));
        mData.add(new ListItem<>("更新云端识别模型-aicar", "updateAsrModel3"));
        mData.add(new ListItem<>("更新云端识别模型-aicommon", "updateAsrModel4"));
        mData.add(new ListItem<>("获取云端识别模型", "getAsrModel"));
        mData.add(new ListItem<>("开启VAD", "enableVad"));
        mData.add(new ListItem<>("关闭VAD", "disableVad"));
        mData.add(new ListItem<>("通过音频获取性别", "getGenderWithPcm"));
        mData.add(new ListItem<>("通过音频获取性别/年龄", "getAsrppWithPcm"));
    }

    public void startListening() {
        //获取识别引擎
        ASREngine asrEngine = DDS.getInstance().getAgent().getASREngine();
        //开启识别
        try {
            asrEngine.startListening(new ASREngine.Callback() {
                @Override
                public void beginningOfSpeech() {
                    Log.i(TAG, "检测到用户开始说话");
                }

                @Override
                public void bufferReceived(byte[] buffer) {
                    Log.i(TAG, "用户说话的音频数据");
                }

                @Override
                public void endOfSpeech() {
                    Log.i(TAG, "检测到用户结束说话");
                }

                @Override
                public void partialResults(String results) {
                    Log.i(TAG, "实时识别结果反馈:" + results);
                }

                @Override
                public void finalResults(String results) {
                    Log.i(TAG, "最终识别结果反馈");
                }

                @Override
                public void error(String error) {
                    Log.i(TAG, "识别过程中发生的错误");
                }

                @Override
                public void rmsChanged(float rmsdB) {
                    Log.i(TAG, "用户说话的音量分贝");
                }
            });
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void stopListening() {
        try {
            //获取识别引擎
            ASREngine asrEngine = DDS.getInstance().getAgent().getASREngine();
            //主动结束此次识别
            asrEngine.stopListening();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void cancelListening() {
        try {
            //获取识别引擎
            ASREngine asrEngine = DDS.getInstance().getAgent().getASREngine();
            asrEngine.cancel();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void disableVolume() {
        // 设置实时回传音量大小, 默认为true
        // 设置false之后, ASREngine.Callback.rmsChanged()不再回传音量变化值
        try {
            DDS.getInstance().getAgent().getASREngine().enableVolume(false);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void enableVolume() {
        // 设置实时回传音量大小, 默认为true
        // 设置false之后, ASREngine.Callback.rmsChanged()不再回传音量变化值
        try {
            DDS.getInstance().getAgent().getASREngine().enableVolume(true);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }

    }

    public void updateAsrModel1() {

        try {
            DDS.getInstance().getAgent().getASREngine().updateAsrModel("aihome");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateAsrModel2() {

        try {
            DDS.getInstance().getAgent().getASREngine().updateAsrModel("airobot");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateAsrModel3() {

        try {
            DDS.getInstance().getAgent().getASREngine().updateAsrModel("aicar");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void updateAsrModel4() {
        try {
            DDS.getInstance().getAgent().getASREngine().updateAsrModel("aicommon");
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getAsrModel() {
        try {
            String asrMode = DDS.getInstance().getAgent().getASREngine().getAsrModel();
            Log.d(TAG, "asrMode:" + asrMode);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void enableVad() {

        try {
            DDS.getInstance().getAgent().getASREngine().enableVad();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void disableVad() {

        try {
            DDS.getInstance().getAgent().getASREngine().disableVad();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getGenderWithPcm() {
        byte[] buffer = AssetsUtil.getFile(this, "xiaole.wav");
        try {
            String genderResult = DDS.getInstance().getAgent().getASREngine().getGenderWithPcm(buffer);
            Log.d(TAG, "genderResult = " + genderResult);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getAsrppWithPcm() {
        // 需要添加此配置才能生效
        // config.addConfig(DDSConfig.K_USE_AGE,"true");
        // config.addConfig(DDSConfig.K_USE_GENDER,"true");
        byte[] buffer = AssetsUtil.getFile(this, "xiaole.wav");
        try {
            String asrppResult = DDS.getInstance().getAgent().getASREngine().getAsrppWithPcm(buffer, ASREngine.AsrppType.GENDER, ASREngine.AsrppType.AGE);
            Log.d(TAG, "asrppResult = " + asrppResult);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

}
