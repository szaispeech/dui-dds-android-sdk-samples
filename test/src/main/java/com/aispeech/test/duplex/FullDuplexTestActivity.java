package com.aispeech.test.duplex;

import android.util.Log;

import com.aispeech.dui.dds.DDS;
import com.aispeech.dui.dds.agent.Agent;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;

public class FullDuplexTestActivity extends BaseTestActivity {

    @Override
    protected void initData() {
        mData.add(new ListItem<>("半双工", "halfDuplex"));
        mData.add(new ListItem<>("全双工", "fullDuplex"));
        mData.add(new ListItem<>("获取dds模式", "getDuplexMode"));
        mData.add(new ListItem<>("全双工模式下,跳过Vad的超时检测","killVadTimeoutInFullDuplex"));
    }

    public void halfDuplex() {
        try {
            DDS.getInstance().getAgent().setDuplexMode(Agent.DuplexMode.HALF_DUPLEX);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void fullDuplex() {
        try {
            DDS.getInstance().getAgent().setDuplexMode(Agent.DuplexMode.FULL_DUPLEX);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getDuplexMode() {
        // 获取dds模式
        try {
            Agent.DuplexMode mode = DDS.getInstance().getAgent().getDuplexMode();
            Log.d(TAG,"DuplexMode:"+mode);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    //ASREngine.killVadTimeoutInFullDuplex(), 在全双工模式下,跳过Vad的超时检测
    //只在当前对话时生效
    public void killVadTimeoutInFullDuplex() {
        try {
            DDS.getInstance().getAgent().getASREngine().killVadTimeoutInFullDuplex();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }



}

