package com.aispeech.test.vprint;

import android.util.Log;

import com.aispeech.dui.dds.agent.vprint.VprintEngine;
import com.aispeech.dui.dds.agent.vprint.VprintListener;
import com.aispeech.dui.dds.exceptions.DDSNotInitCompleteException;
import com.aispeech.test.BaseTestActivity;
import com.aispeech.test.ListItem;
import com.aispeech.libbase.export.bean.VprintIntent;

/**
 * config.addConfig(DDSConfig.K_VPRINT_ENABLE, "true");// 是否开启声纹功能
 * config.addConfig(DDSConfig.K_USE_VPRINT_IN_WAKEUP, "true");// 是否开启声纹功能并在唤醒中使用声纹判断
 * config.addConfig(DDSConfig.K_VPRINT_BIN, "/sdcard/vprint.bin");// 声纹资源绝对路径
 */
public class VprintTestActivity extends BaseTestActivity {
    private int mType = 1;
    public int mOutChannelNum = 2;// 双路
    public int mTrainNum = 3;// 重启次数
    public String mWord = "ni hao xiao chi";

    @Override
    protected void initData() {
        mData.add(new ListItem("单麦声纹", "setMode1"));
        mData.add(new ListItem("双麦声纹", "setMode2"));
        mData.add(new ListItem("线性四麦声纹", "setMode4"));
        mData.add(new ListItem("声纹监听器", "startListener"));
        mData.add(new ListItem("注册声纹", "regist"));
        mData.add(new ListItem("更新声纹", "update"));
        mData.add(new ListItem("追加声纹", "append"));
        mData.add(new ListItem("使用声纹", "test"));
        mData.add(new ListItem("停止声纹", "stop"));
        mData.add(new ListItem("获取声纹模型", "getMode"));
        mData.add(new ListItem("删除一条声纹", "unregister"));
        mData.add(new ListItem("删除所有声纹", "unregisterAll"));
    }

    public void setMode1() {
        mOutChannelNum = 1;
    }

    public void setMode2() {
        mOutChannelNum = 2;
    }

    public void setMode4() {
        mOutChannelNum = 1;
        mType = 4;
    }

    public VprintIntent.Builder getVprintBuilder(VprintIntent.Action action) {
        VprintIntent.Builder builder = new VprintIntent.Builder();
        builder.setAction(action);
        builder.setUserId("dengzi");
        builder.setVprintWord(mWord);
        builder.setTrainNum(mTrainNum);
        if (mOutChannelNum > 1) {
            builder.setOutChannelNum(mOutChannelNum);
        }
        if (mType == 4) {
            builder.setOutChannelNum(mOutChannelNum);
        }
        return builder;
    }

    public void regist() {
        try {
            VprintIntent intent = getVprintBuilder(VprintIntent.Action.REGISTER).setSnrThresh(8.67f).create();

            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            VprintIntent intent = getVprintBuilder(VprintIntent.Action.UPDATE).setSnrThresh(8.67f).create();

            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void append() {
        try {
            VprintIntent intent = getVprintBuilder(VprintIntent.Action.APPEND).setSnrThresh(8.67f).create();

            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void test() {
        try {
            VprintIntent.Builder builder = new VprintIntent.Builder();
            builder.setAction(VprintIntent.Action.TEST);
            if (mOutChannelNum > 1) {
                builder.setOutChannelNum(mOutChannelNum);
            }

            VprintIntent intent = builder.create();
            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            VprintEngine.getInstance().stop();
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void startListener() {
        try {
            VprintEngine.getInstance().setVprintListener(new VprintListener() {
                @Override
                public void onState(String state) {
                    Log.e(TAG, "onState = " + state);
                }

                @Override
                public void onResults(String result) {
                    Log.e(TAG, "onResults = " + result);
                }

                @Override
                public void onError(String error) {
                    Log.e(TAG, "onError = " + error);
                }
            });
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void getMode() {
        try {
            String result = VprintEngine.getInstance().getMode();
            Log.d(TAG, "getMode result = " + result);
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void unregister() {
        try {
            VprintIntent intent = getVprintBuilder(VprintIntent.Action.UNREGISTER).create();

            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

    public void unregisterAll() {
        try {
            VprintIntent intent = new VprintIntent.Builder()
                    .setAction(VprintIntent.Action.UNREGISTER_ALL)
                    .create();
            VprintEngine.getInstance().start(intent);
            toMain();
        } catch (DDSNotInitCompleteException e) {
            e.printStackTrace();
        }
    }

}
