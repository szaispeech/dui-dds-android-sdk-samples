package com.aispeech.test;

import android.content.Intent;

import com.aispeech.test.agent.AgentTestActivity;
import com.aispeech.test.asr.AsrTestActivity;
import com.aispeech.test.duplex.FullDuplexTestActivity;
import com.aispeech.test.increment_wakeup.incrementWakeupActivity;
import com.aispeech.test.tts.TTSTestActivity;
import com.aispeech.test.upload.UploadDataTestActivity;
import com.aispeech.test.vad.VadTestActivity;
import com.aispeech.test.vprint.VprintTestActivity;
import com.aispeech.test.wakeup.WakeupTestActivity;


public class MainTestActivity extends BaseTestActivity {

    @Override
    protected void initData() {
        mData.add(new ListItem<>("唤醒测试", WakeupTestActivity.class));
        mData.add(new ListItem<>("TTS测试", TTSTestActivity.class));
        mData.add(new ListItem<>("ASR测试", AsrTestActivity.class));
        mData.add(new ListItem<>("VAD测试", VadTestActivity.class));
        mData.add(new ListItem<>("声纹测试", VprintTestActivity.class));
        mData.add(new ListItem<>("Agent测试", AgentTestActivity.class));
        mData.add(new ListItem<>("数据上传测试", UploadDataTestActivity.class));
        mData.add(new ListItem<>("本地识别增量", incrementWakeupActivity.class));
        mData.add(new ListItem<>("全双工测试", FullDuplexTestActivity.class));
    }

    @Override
    protected void onItemClick(ListItem item) {
        Intent intent = new Intent(MainTestActivity.this, item.getKlass());
        intent.putExtra("title", item.getTitle());
        startActivity(intent);
    }
}
