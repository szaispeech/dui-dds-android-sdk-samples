package com.aispeech.test;

import org.junit.Test;

import java.nio.Buffer;
import java.nio.ByteBuffer;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        ByteBuffer cache = ByteBuffer.allocate(1024);
        System.out.println(cache.array().length + " "  + cache.remaining() + "  " + cache.hasArray() + "  " + cache.position());
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        cache.put((byte) 1);
        System.out.println(cache.array().length + " "  + cache.remaining() + "  " + cache.hasArray() + "  " + cache.position());
        cache.put((byte) 1);
        System.out.println(cache.array().length+ " "  + cache.remaining());
        byte[] bytes = new byte[123];
        cache.put(bytes);
        System.out.println(cache.array().length + " "  + cache.remaining() + "  " + cache.hasArray() + "  " + cache.position());
        cache.clear();
        System.out.println(cache.array().length + " "  + cache.remaining() + "  " + cache.hasArray());
        byte[] bytes1 = new byte[1024];
        cache.put(bytes1);
        System.out.println(cache.array().length + " "  + cache.remaining() + "  " + cache.hasArray() + "  " + cache.position());
        assertEquals(4, 2 + 2);
    }
}